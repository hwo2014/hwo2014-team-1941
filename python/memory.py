from katyaMind import KatyaMind
import physics
from track import Track
from pieceType import PieceType
from car import Car
from info import Info
from history import History
import copy

maxHistorySize = 10


class Memory:
    def __init__(self):
        self.gameTick = -1
        self.name = ""
        self.color = ""
        self.built = False
        self.katyaMind = KatyaMind()
        
    def build(self, data):
        self.lastDirection = None
        self.throttle = -1
        self.gameTick = -1
        
        if not self.built:
            self.track = Track(data["race"]["track"])

        self.carCount = len(data["race"]["cars"])
        self.cars = []
        for carData in data["race"]["cars"]:
            self.cars.append(Car(carData))

        self.info = Info(data["race"]["raceSession"])
        self.myCarId = self.findCarId(self.name, self.color)
        self.myCar = self.cars[self.myCarId]
    
        self.built = True
        self.expectedCar = None
        
        self.history = []


    def addMemory(self, memory):
        if len(self.history) < maxHistorySize:
            self.history.append(None)
        self.history = [History(copy.deepcopy(memory.cars), memory.throttle, memory.myCarId)]+self.history[:(len(self.history) - 1)]
    
    def update(self, data):
        for carData in data:
            self.findCar(carData["id"]["name"], carData["id"]["color"]).update(carData, self)
        if not self.myCar.crashed and self.myCar.newPiece:
            self.lastDirection = None
            print("piece: %d, gameTick: %d, speed: %.3lf, angle: %.3lf, pieceType: %s" %
                  (self.myCar.pieceId, self.gameTick, self.myCar.speed, self.myCar.angle,
                  "straight" if self.track.pieces[self.myCar.pieceId].type == PieceType.straight else
                  "bend, radius: %.3lf" % self.track.getRadius(self.myCar.getPosition())))
    
    def findCarId(self, name, color):
        for i in range(self.carCount):
            if self.cars[i].name == name and self.cars[i].color == color:
                return i
        return -1

    def findCar(self, name, color):
        carId = self.findCarId(name, color)
        return None if carId == -1 else self.cars[carId]
