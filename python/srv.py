from socket import *
import sys

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("Usage: ./srv indata [port]")
    else:
        fname = sys.argv[1]
        if len(sys.argv) > 2: port = int(sys.argv[2])
        else: port = 2399

        data = open(fname, "r").read()

        s = socket(AF_INET, SOCK_STREAM)
        s.bind(('127.0.0.1', int(port)))
        s.listen(1)
        print("Listening on port %d" % port)

        cs, addr = s.accept()
        cs.settimeout(1)
        cs.send(data)
        while True:
            try:
                cs.recv(1024 * 1024)
            except timeout:
                break
        cs.shutdown(SHUT_RDWR)
        cs.close()
