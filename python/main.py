import json
import socket
import sys
import math
from gameLogic import move
from memory import Memory
from turbo import Turbo
import physics
import traceback
import time

class Katya(object):
    def __init__(self, socket, name, key, inlog = None, outlog = None):
        self.memory = Memory()
        self.socket = socket
        self.name = name
        self.key = key
        self.tournamentEnd = False
        self.inlog = inlog
        self.outlog = outlog

    def msg(self, msg_type, data):
        if self.memory.gameTick == -1:
            self.send(json.dumps({"msgType": msg_type, "data": data}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.memory.gameTick}))
        
    def send(self, msg):
        self.socket.sendall(msg + "\n")
        if self.outlog:
            self.outlog.write(msg + "\n")
            self.outlog.flush()

    
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def ping(self):
        #print("Ping, everybody be cool!")
        self.msg("ping", {})

    def joinRace(self, trackName, carCount, password=""):
        return self.msg("joinRace", {
            "botId": {"name": self.name, "key": self.key},
            "trackName": trackName,
            "password": password,
            "carCount": carCount
        })

    def simpleJoinRace(self, carCount):
        return self.msg("joinRace", {
            "botId": {"name": self.name, "key": self.key},
            "carCount": carCount
        })


    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.memory.throttle = throttle

    def turbo(self):
        print("asking server to launch turbo")
        self.msg("turbo", "run run run fast banana!")

    def switchLane(self, lastDirection):
        print("going to switch: %s" % (lastDirection))
        self.msg("switchLane", lastDirection)
        self.memory.lastDirection = lastDirection

    def run(self, local, track, otherRacers):
        if local:
            tracks = (
              "keimola", #0
              "germany", #1
              "usa", #2
              "france", #3
              "elaeintarha", #4
              "imola", #5
              "england", #6
              "suzuka", #7
              "pentag" #8
              )
            if unicode(track).isnumeric():
              track = tracks[int(track)]
            self.joinRace(track, int(otherRacers))
            #self.simpleJoinRace(2)
        else:
            self.join()
        
        while not self.tournamentEnd:
            self.msg_loop()
    
    def on_game_init(self, data):
        print("Init")
        self.memory.build(data)
            
    def on_join(self, data):
        print("Joined")

    def on_your_car(self, data):
        print("yourCar")
        self.memory.color = data["color"]
        self.memory.name = data["name"]

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.memory.update(data)

        if self.memory.gameTick == -1 or self.memory.myCar.crashed:
            self.ping()
            return

        throttle, direction, turbo = move(self.memory)

        if abs(throttle - self.memory.throttle) > physics.EPS:
            self.throttle(throttle)
            return
        if turbo:
            self.turbo()
            return
        if direction and direction != self.memory.lastDirection:
            self.switchLane(direction)
            return

        self.throttle(throttle)

    def on_crash(self, data):
        print("Someone crashed")
        car = self.memory.findCar(data["name"], data["color"])
        car.crashed = True
        car.refresh()
        if car is self.memory.myCar:
            self.memory.expectedCar = None

    def on_dnf(self, data):
        print("Someone WAS DISQUALIFIED")
        self.on_crash(data["car"])

    def on_spawn(self, data):
        print("Someone spawned")
        car = self.memory.findCar(data["name"], data["color"])
        car.crashed = False

    def on_game_end(self, data):
        print("Race ended")
        
    def on_lap_finished(self, data):
        print("-----     Lap Time: %s     -----" % data["lapTime"]["millis"])

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_tournament_end(self, data):
        print("Tournament end!")
        self.tournamentEnd = True

    def on_finish(self, data):
        print("Finish")
        car = self.memory.findCar(data["name"], data["color"])
        car.crashed = True
    
    def on_turbo_available(self, data):
        print("(.)(.)(.) turbo available!")
        for car in self.memory.cars:
            if not car.crashed and not car.turbo:
                car.turbo = Turbo(data)

    def on_turbo_start(self, data):
        car = self.memory.findCar(data["name"], data["color"])
        car.startTurbo()
        if car is self.memory.myCar:
            print(">>>> TURBO ON >>>>")


    def on_turbo_end(self, data):
        car = self.memory.findCar(data["name"], data["color"])
        car.endTurbo()
        if car is self.memory.myCar:
            print(">>>> TURBO OFF >>>>")

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
            'tournamentEnd': self.on_tournament_end,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'finish': self.on_finish,
            'dnf': self.on_dnf,
        }

        socket_file = s.makefile()
        line = socket_file.readline()
        if self.inlog:
            self.inlog.write(line)
            self.inlog.flush()
        
        last_time = time.time()
        while line:
            start_time = time.time()
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if "gameTick" in msg:
                self.memory.gameTick = int(msg["gameTick"])
                #print("Game tick %d" % (self.memory.gameTick))
            else:
                self.memory.gameTick = -1

            try:
                if msg_type in msg_map:
                    msg_map[msg_type](data)
                else:
                    print("Got {0}".format(msg_type))
            except:
                traceback.print_exc()
                print("Sending emergency throttle=0.2!")
                self.throttle(0.2)
            sys.stdout.flush()
            
            print("Time = %.3f msec, since last = %.3f msec" % ((time.time() - start_time) * 1000, (time.time() - last_time) * 1000))
            
            last_time = time.time()

            line = socket_file.readline()
            if self.inlog:
                self.inlog.write(line)
                self.inlog.flush()

if __name__ == "__main__":
    if len(sys.argv) < 5 or len(sys.argv) > 9:
        print("Usage: ./run host port botname botkey [bananakey] [track] [number of other racers] [logfile]")
    else:
        host, port, name, key = sys.argv[1:5]
        local = (len(sys.argv) >= 6)
        if local and sys.argv[5] != "bananaKey":
            print("They are hacking us!")
        else:
            print("Connecting with parameters:")
            print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            s.connect((host, int(port)))
    
            inlog = None
            outlog = None
            if (host != "localhost") and local:
                import datetime
                logname = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S.indata')
                if len(sys.argv) > 8: logname=sys.argv[8]
                if logname.endswith('indata'):
                    logname=logname[0:len(logname)-7]
                inlog = open(logname+".indata", "w")
                outlog = open(logname+".outdata", "w")

            bot = Katya(s, name, key, inlog=inlog, outlog=outlog)
            track = sys.argv[6] if len(sys.argv) > 6 else "usa"
            otherRacers = sys.argv[7] if len(sys.argv) > 7 else 1
            bot.run(local, track, otherRacers) 
