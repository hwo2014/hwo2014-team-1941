from pieceType import PieceType
import physics
from car import Car
from position import Position
import values
import copy

def thinkAboutTurbo(memory):
    if not memory.myCar.turbo or memory.myCar.onTurbo:
        return False
    pieceId = memory.myCar.pieceId
    if memory.track.pieces[pieceId - 1].type == PieceType.straight:
        return False
    while pieceId < memory.track.pieceCount and memory.track.pieces[pieceId].type == PieceType.straight:
        pieceId += 1
    if pieceId == memory.track.pieceCount:
        return True
    return False

def thinkAboutSwitch(car, track):
    if not track.pieces[car.pieceId + 1].switch:
        return None
    lane = car.endLaneIndex
    angle = 0
    pieceId = car.pieceId + 2
    while not track.pieces[pieceId].switch:
        angle += track.pieces[pieceId].angle
        pieceId += 1
    if lane > 0 and angle < -0.02:
        return "Left"
    if lane < track.laneCount - 1 and angle > 0.02:
        return "Right"

    #if -0.02 <= angle <= 0.02 and car.angle > 0.5 and lane > 0:
    #    return "Left"
    #if -0.02 <= angle <= 0.02 and car.angle < -0.5 and lane < track.numberOfLanes - 1:
    #    return "Right"
    return None


maxCalc = 0

def canSlowDown(car, memory):
    global maxCalc
    calc = 0
    while True:
        calc += 1
        maxCalc = max(maxCalc, calc)
        if car.speed < 2.0:
            return True
        if abs(car.angle) >= physics.maxAngle - 1.0 - physics.EPS:
            return False
        if car.lap == memory.info.laps:
            return True
        if not physics.ready():
            if memory.track.pieces[car.pieceId].type == PieceType.bend\
            and car.speed > physics.findMaxSpeed(memory.track.getRadius(car.getPosition())) + physics.EPS:
                return False
        physics.emulate(car, memory.track, 0)

def findThrottle(memory):
    print("maxSpeedVal = %.10lf" % (physics.maxSpeedVal))
    car = copy.deepcopy(memory.myCar)
    if memory.track.pieces[car.pieceId].type == PieceType.bend:
        t = physics.wantThisSpeed(car, physics.findMaxSpeed(memory.track.getRadius(car.getPosition())))
    else:
        t = 1.0
    physics.emulate(car, memory.track, t)
    if canSlowDown(car, memory):
        return t
    
    return 0.0

def diff(car1, car2):
    if abs(car1.pos - car2.pos) > physics.EPS:
        print("pos doesn't match! %.10lf != %.10lf   diff: %.10lf" %
              (car1.pos, car2.pos, abs(car1.pos - car2.pos)))
    if abs(car1.speed - car2.speed) > physics.EPS:
        print("speed doesn't match! %.10lf != %.10lf   diff: %.10lf" %
              (car1.speed, car2.speed, abs(car1.speed - car2.speed)))
    if abs(car1.angle - car2.angle) > physics.EPS:
        print("angle doesn't match! %.10lf != %.10lf   diff: %.10lf" %
              (car1.angle, car2.angle, abs(car1.angle - car2.angle)))
    if abs(car1.dAngle - car2.dAngle) > physics.EPS:
        print("dAngle doesn't match! %.10lf != %.10lf   diff: %.10lf" %
              (car1.dAngle, car2.dAngle, abs(car1.dAngle - car2.dAngle)))
 

def move(memory):
    memory.addMemory(memory)
    if memory.expectedCar:
        diff(memory.myCar, memory.expectedCar)
        if physics.speedReady() and abs(memory.myCar.pos - memory.expectedCar.pos) > physics.EPS:
            print("Maybe we don't feel the Power")
            memory.history = []
    else:
        memory.expectedCar = Car()
    
    values.updateValues(memory)
    
    if physics.ready():
        throttle, direction, turbo = memory.katyaMind.move(memory)
    else:
        throttle = findThrottle(memory)
        direction = thinkAboutSwitch(memory.myCar, memory.track)
        turbo = thinkAboutTurbo(memory)

    memory.expectedCar = copy.deepcopy(memory.myCar)
    physics.emulate(memory.expectedCar, memory.track, throttle)

    #if not canSlowDown(copy.deepcopy(memory.myCar), memory):
    #    print("i'm gonna to fly or to bump somebody")

    print("passed gameTick %d:  pos = %.10lf, speed = %.10lf, throttle = %.10lf, direction = %s, angle = %.10lf, isSwitch = %s" %
          (memory.gameTick, memory.myCar.pos, memory.myCar.speed, throttle, direction, memory.myCar.angle, memory.myCar.startLaneIndex != memory.myCar.endLaneIndex))
    print("pos = %.10lf, speed = %.10lf, throttle = %.10lf, angle = %.10lf, dAngle = %.10lf, length = %.10lf (%d %d)" %
          (memory.myCar.pos, memory.myCar.speed, throttle, memory.myCar.angle, memory.myCar.dAngle,
          memory.track.getLength(memory.myCar.getPosition()), memory.myCar.startLaneIndex, memory.myCar.endLaneIndex))

    return throttle, direction, turbo
