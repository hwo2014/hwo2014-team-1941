import math
from pieceType import PieceType
from car import Car

speedA = None
speedB = None

def speedReady():
    return speedA and speedB

EPS = 10 ** (-7)
INF = 10 ** (9)
              
def getNextSpeed(car, throttle):
    if not speedReady():
        return 0
    return speedA * car.speed + speedB * car.enginePower * throttle

angleA = None
angleB = None

def angleStraightReady():
    return angleA and angleB

def getNextAngleSpeed(car, equilibriumAngle = 0):
    if not angleStraightReady():
        return -car.angle
    return angleA * car.dAngle + angleB * car.speed * (car.angle - equilibriumAngle)

angleV = None
angleDV = None

def angleDriftReady():
    return angleV and angleDV

def ready():
    return speedReady() and angleStraightReady() and angleDriftReady()

def getNextDAngle(car, track):
    if track.pieces[car.pieceId].type == PieceType.straight:
        return getNextAngleSpeed(car)
    else:
        if not angleDriftReady():
            return -car.angle
        R = track.getRadius(car.getPosition())
        Vmin = (R * angleV) ** 0.5
        dV = (angleDV / R) ** 0.5
        equilibriumAngle = max(0, (car.speed - Vmin)) * dV
        if track.pieces[car.pieceId].angle < 0:
            equilibriumAngle *= -1
        return getNextAngleSpeed(car, equilibriumAngle)

maxAngle = 60
safeCoef = 0.8

def emulate(car, track, throttle):
    car.dAngle = getNextDAngle(car, track)
    car.angle += car.dAngle
    car.speed = getNextSpeed(car, throttle)
    car.pos += car.speed
    length = track.getLength(car.getPosition())
    car.newPiece = False
    if car.pos >= length:
        car.pos -= length
        car.pieceId += 1
        car.startLaneIndex = car.endLaneIndex
        car.newPiece = True
        if car.pieceId == track.pieceCount:
            car.lap += 1
            car.pieceId = 0



def wantThisSpeed(car, need):
    left = speedA * car.speed
    right = speedA * car.speed + speedB * car.enginePower
    if need < left:
        need = left
    if need > right:
        need = right
    result = (need - speedA * car.speed) / (speedB * car.enginePower)
    #print("RESULT = %.10lf" % result)
    result = max(0.0, min(1.0, result))
    return result

def countEquilibriumAngle(newDAngle, dAngle, angle, speed):
    assert(angleStraightReady())
    return angle - (newDAngle - angleA * dAngle) / speed / angleB

delta = 0.05
maxSpeedVal = 0.25
def findMaxSpeed(radius):
    return maxSpeedVal * math.sqrt(radius)
