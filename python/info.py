class Info:
    def __init__(self, data):
        if "laps" in data:
            self.laps = data["laps"]
        else:
            self.laps = 1000
        if "maxLapTimeMs" in data:
            self.maxLapTimeMs = data["maxLapTimeMs"]
        self.quickRace = (("quickRace" in data) and data["quickRace"])
