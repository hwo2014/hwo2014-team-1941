import math
import physics
from pieceType import PieceType
from position import Position
    
class Piece:
    def __init__(self, pieceInfo):
        self.type = PieceType.straight if ("length" in pieceInfo) else PieceType.bend
        if "switch" in pieceInfo:
            self.switch = (pieceInfo["switch"] == True)
        else:
            self.switch = False
        if self.type == PieceType.straight:
            self.centerLength = pieceInfo["length"]
            self.angle = 0
        else:
            self.angle = pieceInfo["angle"] * math.pi / 180.0
            self.radius = pieceInfo["radius"]
            self.centerLength = abs(self.angle) * self.radius
    
class Track:
    def __init__(self, data):
        self.id = data["id"]
        self.name = data["name"]
        
        self.pieceCount = len(data["pieces"])
        self.pieces = []
        for i in range(self.pieceCount):
            self.pieces.append(Piece(data["pieces"][i]))
        self.pieces[:0] = self.pieces


        self.laneCount = len(data["lanes"])
        self.lanes = {}
        for i in range(self.laneCount):
            laneInfo = data["lanes"][i]
            assert(0 <= laneInfo["index"] < self.laneCount)
            self.lanes[laneInfo["index"]] = laneInfo["distanceFromCenter"]

        self.straightLengthDict = {}
        self.bendLengthDict = {}
        self.calcTurboPieces()            


    def calcTurboPieces(self):
        self.turboPieces = set()
        mx = 0
        i = 0
        while i < self.pieceCount:
            if self.pieces[i].type == PieceType.bend:
                i += 1
                continue
            j = i
            S = 0
            while self.pieces[j].type == PieceType.straight:
                S += self.pieces[j].centerLength
                j += 1
            if S > mx + physics.EPS:
                self.turboPieces = set()
                mx = S
            if abs(S - mx) < physics.EPS:
                self.turboPieces.add(i)
            i = j
        print(self.turboPieces)
        
    
    def getRadius(self, position):
        piece = self.pieces[position.pieceId]
        assert(abs(piece.angle) > physics.EPS) 
        rStart = piece.radius + (1.0 if piece.angle < 0 else -1.0) * self.lanes[position.startLaneIndex]
        rEnd = piece.radius + (1.0 if piece.angle < 0 else -1.0) * self.lanes[position.endLaneIndex]
        return rStart + (rEnd - rStart) * (position.pos / self.getLength(position))

    def changeStraightLength(self, key, newValue):
        self.straightLengthDict[key] = newValue
 
    def changeBendLength(self, key, newValue):
        print(key)
        self.bendLengthDict[key] = newValue

    def straightDictKey(self, pieceId, startLaneIndex, endLaneIndex):
        return self.pieces[pieceId].centerLength, abs(self.lanes[startLaneIndex] - self.lanes[endLaneIndex])
    
    def bendDictKey(self, pieceId, startLaneIndex, endLaneIndex):
        r1 = self.getRadius(Position(pieceId, 0, startLaneIndex, startLaneIndex))
        r2 = self.getRadius(Position(pieceId, 0, endLaneIndex, endLaneIndex))
        if r1 > r2:
            tmp = r1
            r1 = r2
            r2 = tmp
        return abs(self.pieces[pieceId].angle), r1, r2
            
    def changeLength(self, pieceId, startLaneIndex, endLaneIndex, toadd):
        if abs(toadd) < physics.EPS:
            return
        assert(startLaneIndex != endLaneIndex)
        newValue = toadd + self.getLength(Position(pieceId, 0, startLaneIndex, endLaneIndex))
        print("change length %d %d %d (center = %.5lf, angle = %.5lf)    from %.5lf to %.5lf" % (pieceId, startLaneIndex, endLaneIndex, self.pieces[pieceId].centerLength, self.pieces[pieceId].angle, newValue - toadd, newValue))
        if self.pieces[pieceId].type == PieceType.straight:
            self.changeStraightLength(self.straightDictKey(pieceId, startLaneIndex, endLaneIndex), newValue)
        else:
            self.changeBendLength(self.bendDictKey(pieceId, startLaneIndex, endLaneIndex), newValue)

    def updateLength(self, lastPosition, pos, expectedPos):
        self.changeLength(lastPosition.pieceId, lastPosition.startLaneIndex, lastPosition.endLaneIndex, expectedPos - pos)

    def getLength(self, position):
        piece = self.pieces[position.pieceId]
        length = piece.centerLength

        if piece.type == PieceType.straight:
            if position.startLaneIndex == position.endLaneIndex:
                return length
            key = self.straightDictKey(position.pieceId, position.startLaneIndex, position.endLaneIndex)
            if key in self.straightLengthDict:
                return self.straightLengthDict[key]
            
            # don't know yet
            return length
            
        
        length -= piece.angle * (self.lanes[position.startLaneIndex] + self.lanes[position.endLaneIndex]) * 0.5
        if position.startLaneIndex == position.endLaneIndex:
            return length

        key = self.bendDictKey(position.pieceId, position.startLaneIndex, position.endLaneIndex)
        
        if key in self.bendLengthDict:
            return self.bendLengthDict[key]
        
        # don't know yet
        return length
            
    
    def getDistanceBetween(self, car1, car2):
        return min(car1.getDistanceTo(car2, self), car2.getDistanceTo(car1, self))

    def unknown(self, position):
        if position.startLaneIndex == position.endLaneIndex:
            return False
        piece = self.pieces[position.pieceId]
        if piece.type == PieceType.straight:
            key = self.straightDictKey(position.pieceId, position.startLaneIndex, position.endLaneIndex)
            if key in self.straightLengthDict:
                return False
        return True
            