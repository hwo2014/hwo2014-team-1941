from pieceType import PieceType
from position import Position
import copy
import physics

class KatyaMind:
    def __init__(self):
        self.bumps = 0
        self.nextCarId = None

    def changeMind(self, track, car):
        return True
        return car.speed * 2.2 <= track.getLength(car.getPosition()) - car.pos + physics.EPS

    def getSwitches(self, memory, car, lastDirection):
        res = {}
        lane = car.endLaneIndex
        for i in range(car.pieceId + 1, car.pieceId + memory.track.pieceCount):
            j = i % memory.track.pieceCount
            res[j] = None
            if memory.track.pieces[j].switch:
                res[j] = self.isSwitchExpected(memory.track, j, lane)
                if i == car.pieceId + 1:
                    if not self.changeMind(memory.track, car) or not res[j]:
                        res[j] = lastDirection
                
                if res[j] == "Right":
                    lane += 1
                if res[j] == "Left":
                    lane -= 1
        return res
    
    def canSlowDown(self, memory, car, futureNextCar, switches):
        car = copy.deepcopy(car)
        tick = 1
        wasCollide = False
        wasUnknown = memory.track.unknown(car.getPosition())
        maxAngle = physics.maxAngle - physics.EPS

        while True:
            if car.newPiece and car.pieceId in switches:
                if switches[car.pieceId] == "Right":
                    car.endLaneIndex += 1
                if switches[car.pieceId] == "Left":
                    car.endLaneIndex -= 1
                assert(0 <= car.endLaneIndex < memory.track.laneCount)

            if not wasUnknown and memory.track.unknown(car.getPosition()):
                wasUnknown = True

            if not wasUnknown and abs(car.angle) >= maxAngle:
                return False
            if wasUnknown and abs(car.angle) >= maxAngle * physics.safeCoef:
                return False

            if car.speed <= 1.0:
                return True
            if car.lap >= memory.info.laps:
                return True

            physics.emulate(car, memory.track, 0)
            tick += 1
            if not wasCollide and len(futureNextCar) > tick and car.strictlyCollide(futureNextCar[tick], memory.track):
                car.speed = min(car.speed, car.speed * (1 - physics.speedA) + futureNextCar[tick].speed)
                dSpeed = max(5, abs(car.speed - futureNextCar[tick].speed))
                if car.angle < 0: car.angle -= dSpeed
                if car.angle > 0: car.angle += dSpeed
                wasCollide = True




    def getThrottle(self, memory, myCar, futureNextCar):
        switches = self.getSwitches(memory, myCar, memory.lastDirection)
        
        car = copy.deepcopy(myCar)
        physics.emulate(car, memory.track, 1.0)
        if self.canSlowDown(memory, car, futureNextCar, switches):
            return 1.0
        # NEW FUCKING GOOD FEATURE
        elif True:
            return 0.0

        # OLD CODE
        car = copy.deepcopy(myCar)
        physics.emulate(car, memory.track, 0.5 ** 40)
        if not self.canSlowDown(memory, car, futureNextCar, switches):
            return 0.0

        left = 0.0
        right = 1.0
        for it in range(1):
            middle = (left + right) * 0.5
            car = copy.deepcopy(myCar)
            physics.emulate(car, memory.track, middle)
            if self.canSlowDown(memory, car, futureNextCar, switches):
                left = middle
            else:
                right = middle

        return left

    def isSwitchExpected(self, track, pieceId, laneId):
        if not track.pieces[pieceId].switch:
            return None
        angle = 0
        pieceId += 1
        while not track.pieces[pieceId].switch:
            angle += track.pieces[pieceId].angle
            pieceId += 1
        if laneId > 0 and angle < -0.02:
            return "Left"
        if laneId < track.laneCount - 1 and angle > 0.02:
            return "Right"
        return None

    # not bad == maybe it bad decision, but, i'm not loosing much
    # used for creating opportunity for overtaking

    def switchAnywhere(self, track, car):
        if not track.pieces[car.pieceId + 1].switch:
            return None
        lane = car.endLaneIndex
        angle = 0
        pieceId = car.pieceId + 2
        while not track.pieces[pieceId].switch:
            angle += track.pieces[pieceId].angle
            pieceId += 1
        if lane > 0 and angle < 0.02: # all diff is here!
            return "Left"
        if lane < track.laneCount - 1 and angle > -0.02: # all diff is here!
            return "Right"
        if lane > 0:
            return "Left"
        if lane < track.laneCount - 1:
            return "Right"
        return None



    def switchIfGood(self, track, car):
        if not track.pieces[car.pieceId + 1].switch:
            return None
        lane = car.endLaneIndex
        angle = 0
        pieceId = car.pieceId + 2
        while not track.pieces[pieceId].switch:
            angle += track.pieces[pieceId].angle
            pieceId += 1
        if lane > 0 and angle < -0.02:
            return "Left"
        if lane < track.laneCount - 1 and angle > 0.02:
            return "Right"
        return None


    def getDirection(self, memory, myCar, futureNextCar):
        if not memory.track.pieces[myCar.pieceId + 1].switch or not self.changeMind(memory.track, myCar):
            return None
        direction = self.switchIfGood(memory.track, myCar)
        if futureNextCar:
            nextCar = futureNextCar[0]
            if nextCar.pieceId != myCar.pieceId and nextCar.startLaneIndex == nextCar.endLaneIndex and self.bumps >= 2:
                # nextCar is passive and not changing line... it's his problem!
                newDirection = self.switchAnywhere(memory.track, myCar)
                car = copy.deepcopy(myCar)
                physics.emulate(car, memory.track, memory.throttle)
                if newDirection and self.canSlowDown(memory, car, [], self.getSwitches(memory, car, newDirection)):
                    direction = newDirection
        return direction

    def getTurbo(self, memory, myCar, futureNextCar):
        if not myCar.turbo or myCar.onTurbo:
            return False

        car = copy.deepcopy(myCar)
        tick = 0
        car.startTurbo()
        for it in range(car.turbo.turboDurationTicks):
            physics.emulate(car, memory.track, 1.0)
            tick += 1
            if abs(car.angle) > physics.maxAngle - 1:
                return False
            if car.pieceId in memory.track.turboPieces:
                break
        car.endTurbo()

        switches = self.getSwitches(memory, car, memory.lastDirection)
        newFutureNextCar = []
        for i in range(tick, len(futureNextCar)):
            newFutureNextCar.append(futureNextCar[i])
        if self.canSlowDown(memory, car, newFutureNextCar, switches):
            return True

        return False

    def move(self, memory):
        myCar = copy.deepcopy(memory.myCar)

        self.onSwitch = myCar.startLaneIndex != myCar.endLaneIndex

        nextCar = None
        distToNextCar = 300
        for car in memory.cars:
            if car.color != myCar.color and not car.crashed and car.startLaneIndex == car.endLaneIndex:
                dist = myCar.getDistanceTo(car, memory.track)
                if dist < distToNextCar:
                    distToNextCar = dist
                    nextCar = car

        futureNextCar = []

        if memory.expectedCar.speed - myCar.speed > physics.EPS:
            print("expected: %.10lf, got: %.10lf" % (memory.expectedCar.speed, myCar.speed))
            self.bumps += 1

        if nextCar and memory.lastDirection:
            nextCar = None

        nextCar = None
        if nextCar:
            ID = (nextCar.name, nextCar.color)
            if ID != self.nextCarId:
                self.bumps = 0
                self.nextCarId = ID

            nextCar = copy.deepcopy(nextCar)

            for i in range(100):
                futureNextCar.append(copy.deepcopy(nextCar))
                physics.emulate(nextCar, memory.track, 1.0)
                if nextCar.newPiece and memory.track.pieces[nextCar.pieceId].switch:
                    break
        else:
            self.bumps = 0
            self.nextCarId = None

        throttle  = self.getThrottle(memory, myCar, futureNextCar)
        direction = None
        turbo = False
        direction = self.getDirection(memory, myCar, futureNextCar)
        turbo     = self.getTurbo(memory, myCar, futureNextCar)

        car = copy.deepcopy(memory.myCar)
        physics.emulate(car, memory.track, throttle)
        if not self.canSlowDown(memory, car, futureNextCar, self.getSwitches(memory, myCar, memory.lastDirection)):
            print("Fail!")

        return throttle, direction, turbo
