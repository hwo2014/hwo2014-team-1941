import physics

class Matrix:
    def __init__(self, h, w):
        self.h = h
        self.w = w
        self.a = []
        for r in range(h):
            row = []
            for c in range(w):
                row.append(0)
            self.a.append(row)

def solve(a, b):
    n = len(a.a)
    assert(n == len(b.a))
    for i in range(n):
        assert(n == len(a.a[i]) and len(b.a[i]) == 1)
    
    used = [False] * n
    for i in range(n):
        found = False
        row = -1
        for r in range(0, n):
            if not used[r] and abs(a.a[r][i]) > physics.EPS:
                found = True
                row = r
                break
        if not found:
            return None
        
        used[row] = True
        tmp = a.a[row][i]
        for c in range(n):
            a.a[row][c] /= tmp
        b.a[row][0] /= tmp

        for r in range(0, n):
            if r == row:
                continue
            tmp = a.a[r][i]
            for c in range(n):
                a.a[r][c] -= tmp * a.a[row][c]
            b.a[r][0] -= tmp * b.a[row][0]
    
    c = Matrix(n, 1)
    for i in range(n):
        for j in range(n):
            if (abs(a.a[i][j]) > physics.EPS):
                c.a[j][0] = b.a[i][0]
    return c