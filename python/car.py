import physics
from pieceType import PieceType
from position import Position

class Car:
    def refresh(self):
        self.enginePower = 1.0
        self.turbo = None
        self.onTurbo = False
        self.knowPast = False

        self.dAngle = 0
        self.speed = 0
    
    def __init__(self, data=None):
        self.speed = 0
        if not data:
            return

        self.name = data["id"]["name"]
        self.color = data["id"]["color"]
        self.length = data["dimensions"]["length"]
        self.width = data["dimensions"]["width"]
        self.guideFlagPosition = data["dimensions"]["guideFlagPosition"]

        self.pieceId = -1
        self.crashed = False
        
        self.pos = 0
        self.startLaneIndex = -1
        self.endLaneIndex = -1
        self.lap = -1
        self.angle = 0

        self.newPiece = True

        self.refresh()
        self.position = self.getPosition()

    def startTurbo(self):
        self.enginePower *= self.turbo.turboFactor
        self.onTurbo = True

    def endTurbo(self):
        self.enginePower /= self.turbo.turboFactor
        self.onTurbo = False
        self.turbo = None

    def update(self, data, memory):
        if self.crashed:
            return

        newAngle = data["angle"]
        newPieceId = data["piecePosition"]["pieceIndex"]
        newPos = data["piecePosition"]["inPieceDistance"]
        newStartLaneIndex = data["piecePosition"]["lane"]["startLaneIndex"]
        newEndLaneIndex = data["piecePosition"]["lane"]["endLaneIndex"]
        newLap = data["piecePosition"]["lap"]
        if self.knowPast:
            if self.pieceId == newPieceId:
                newSpeed = newPos - self.pos
            else:
                if memory.myCar is self and not self.couldBump(memory.track, memory.cars):
                    memory.track.updateLength(self.getPosition(), newPos, memory.expectedCar.pos)
                newSpeed = memory.track.getLength(self.getPosition()) - self.pos + newPos

            newDAngle = newAngle - self.angle
            self.newPiece = (newPieceId != self.pieceId)
        else:
            newDAngle = 0
            newSpeed = 0

        self.angle = newAngle
        self.dAngle = newDAngle

        self.pieceId = newPieceId
        self.pos = newPos
        self.speed = newSpeed

        self.startLaneIndex = newStartLaneIndex
        self.endLaneIndex = newEndLaneIndex
        self.lap = newLap

        self.position = self.getPosition()

        if self is memory.myCar and not physics.ready() and memory.track.pieces[self.pieceId].type == PieceType.bend:
            if abs(self.angle) <= physics.EPS and self.speed >= physics.findMaxSpeed(memory.track.getRadius(self.getPosition())) - physics.EPS:
                physics.maxSpeedVal += 0.005
            if physics.speedReady() and physics.angleStraightReady():
                physics.maxSpeedVal += 0.0005
        
        self.knowPast = True
        
    def getPosition(self):
        return Position(self.pieceId, self.pos, self.startLaneIndex, self.endLaneIndex)

    def getDistanceTo(self, car, track):
        if self.pieceId == car.pieceId:
            if self.startLaneIndex != car.startLaneIndex or self.endLaneIndex != car.endLaneIndex:
                return physics.INF
            if self.pos >= car.pos - physics.EPS:
                return physics.INF
            return car.pos - self.pos

        if self.endLaneIndex != car.startLaneIndex:
            return physics.INF
        
        p1 = self.pieceId
        p2 = car.pieceId
        if p2 < p1:
            p2 += track.pieceCount
        if p1 + 10 < p2:
            return physics.INF
        res = track.getLength(self.position) - self.pos
        for i in range(p1 + 1, p2):
            res += track.getLength(Position(i, 0, self.endLaneIndex, self.endLaneIndex))
        res += car.pos
        return res

    
    def couldBump(self, track, cars):
        for car in cars:
            if self is car:
                continue
            dist = track.getDistanceBetween(self, car)
            C = (self.length + car.length) * 0.5 * 2.0
            print("%.3lf %.3lf" % (dist, C))
            if dist < C:
                return True
        return False

    def strictlyCollide(self, car2, track):
        C = (self.length + car2.length) * 0.5 * 1.15
        return track.getDistanceBetween(self, car2) < C
