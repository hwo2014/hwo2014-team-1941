class Turbo:
    def __init__(self, data):
        self.turboDurationMilliseconds = data["turboDurationMilliseconds"]
        self.turboDurationTicks = data["turboDurationTicks"]
        self.turboFactor = data["turboFactor"]
        #print("turbo factor = %.3lf" % self.turboFactor)
