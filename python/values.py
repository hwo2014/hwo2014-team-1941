import physics
import linear
import math

def updateSpeed(memory):
    if not (physics.speedA is None):
        return
    assert(physics.speedB is None)
    need = 2
    if len(memory.history) <= need:
        return

    A = linear.Matrix(need, 2)
    B = linear.Matrix(2, 1)
    for r in range(need):
        car = memory.history[r + 1].myCar
        A.a[r][0] = car.speed
        A.a[r][1] = memory.history[r].throttle
        B.a[r][0] = memory.history[r].myCar.speed
    
    C = linear.solve(A, B)
    if not C:
        return
    physics.speedA = C.a[0][0]
    physics.speedB = C.a[1][0]
    print("update speed! %.3lf %.3lf" % (physics.speedA, physics.speedB))

def updateAngleStraight(memory):
    if not (physics.angleA is None):
        return
    assert(physics.angleB is None)
    
    need = 3
    if len(memory.history) <= need:
        return

    wholeSpeed = None
    wholeAngle = None
    for i in range(need):
        if abs(memory.history[i].myCar.angle) <= physics.EPS:
            return
        
        currentSpeed = memory.history[i + 1].myCar.speed
        if wholeSpeed is None:
            wholeSpeed = currentSpeed
        else:
            if abs(wholeSpeed - currentSpeed) > physics.EPS:
                return

        currentCar = memory.history[i + 1].myCar
        currentAngle = memory.track.pieces[currentCar.pieceId].angle
        if wholeAngle is None:
            wholeAngle = currentAngle
        else:
            if abs(wholeAngle - currentAngle) > physics.EPS:
                return

        if currentCar.startLaneIndex != currentCar.endLaneIndex:
            return
    A = linear.Matrix(need, 3)
    B = linear.Matrix(3, 1)
    for r in range(need):
        car = memory.history[r + 1].myCar
        A.a[r][0] = car.dAngle
        A.a[r][1] = car.speed * car.angle
        A.a[r][2] = 1
        B.a[r][0] = memory.history[r].myCar.dAngle
    print(A.a)
    print(B.a)
    C = linear.solve(A, B)
    if C is None:
        return
    physics.angleA = C.a[0][0]
    physics.angleB = C.a[1][0]
    print("update angle straight! (%.10lf %.10lf)" % (physics.angleA, physics.angleB))
    print("and eq = %.10lf" % (C.a[2][0]))

def updateAngleDrift(memory):
    if not (physics.angleV is None):
        return
    assert(physics.angleDV is None)

    if not physics.angleStraightReady():
        return


    need = 2
    if len(memory.history) <= need:
        return

    for i in range(need):
        car = memory.history[i + 1].myCar
        if abs(physics.countEquilibriumAngle(memory.history[i].myCar.dAngle, car.dAngle, car.angle, car.speed)) <= physics.EPS:
            return                                                                               
        if car.startLaneIndex != car.endLaneIndex:
            return

    A = linear.Matrix(need, 2)
    B = linear.Matrix(2, 1)
    for r in range(need):
        car = memory.history[r + 1].myCar
        sign = (-1 if memory.track.pieces[car.pieceId].angle < 0 else 1)
        A.a[r][0] = sign * car.speed / math.sqrt(memory.track.getRadius(car.getPosition()))
        A.a[r][1] = -sign
        B.a[r][0] = physics.countEquilibriumAngle(memory.history[r].myCar.dAngle, car.dAngle, car.angle, car.speed)
    print(A.a)
    print(B.a)
    C = linear.solve(A, B)
    if C is None:
        return
    physics.angleDV = C.a[0][0]
    assert(abs(C.a[0][0]) > physics.EPS)
    physics.angleV = C.a[1][0] / C.a[0][0]
    physics.angleDV *= physics.angleDV
    physics.angleV *= physics.angleV
    print("update angle drift! (%.5lf %.5lf)" % (physics.angleDV, physics.angleV))

def updateValues(memory):
    updateSpeed(memory)
    updateAngleStraight(memory)
    updateAngleDrift(memory)
